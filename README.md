# Challenge CH 8 #

* Kriteria 1 Membuat dokumentasi API dengan Swagger

Kriteria Tugas ada di direktori utama, repo tidak ada yang diubah kecuali penambahan Swagger.
Dokumentasi Swagger ada di: http://localhost:5000/api-docs

* Kriteria 2 Membuat aplikasi client-side dengan React.js

    - Client ada di folder client\appch8
    - Port yang digunakan bawaan create-react-app yaitu port 3000 [ http://localhost:3000 ]
    - Fitur Search belum diimplementasikan
    - kode masih menggunakan <this.refs> dengna string yang menurut ESLint sudah deprecated, masih belum berhasil menggunakan refs baru React (React.createRef())